Element Multiple
================

This module provides a form element to assist in creation of multiple elements.

### INSTALLATION
Install as usual, see [Installing Drupal 8 Modules](https://www.drupal.org/node/1897420) or [Installing modules' Composer dependencies](https://www.drupal.org/node/2627292) for further information.

### CONFIGURATION
No configuration needed.

### USAGE
```php
$form['multiple'] = [
  '#type' => 'element_multiple',
  '#title' => 'Multiple values',
  '#header' => [
    ['data' => $this->t('First name'), 'width' => '50%'],
    ['data' => $this->t('Second name'), 'width' => '50%'],
  ],
  '#element' => [
    'first_name' => [
      '#type' => 'textfield',
      '#title' => $this->t('First name'),
    ],
    'last_name' => [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
    ],
  ],
  '#default_value' => $config->get('multiple'),
];
```

### CREDITS AND THANKS
The Drupal 8/9 version of this module is heavily based on the [Webform module](https://www.drupal.org/project/webform).

### SPONSORS
- [Amara S.A.](https://es.amara-e.com/)

### CONTACT
Developed and maintained by Cambrico ([http://cambrico.net](http://cambrico.net)).

Get in touch with us for customizations and consultancy: [http://cambrico.net/contact](http://cambrico.net/contact)

#### Current maintainers:
  - Pedro Cambra [(pcambra)](http://drupal.org/user/122101)
  - Manuel Egío [(facine)](http://drupal.org/user/1169056)
