<?php

namespace Drupal\element_multiple_test\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\element_multiple_test\TestElementMultipleFormBase;

/**
 * Provides a test form for element multiple header.
 */
class TestElementMultipleHeaderForm extends TestElementMultipleFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_element_multiple_header';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['element_multiple_basic'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_basic',
    ];
    $form['element_multiple_basic_header_string'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_basic_header_string',
      '#header' => '{element_multiple_basic_header_string}',
    ];
    $form['element_multiple_basic_header_label'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_basic_header_label',
      '#header_label' => '{element_multiple_basic_header_label}',
    ];
    $form['element_multiple_elements'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements',
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
        ],
        'email' => [
          '#type' => 'email',
          '#title' => 'email',
        ],
      ],
    ];
    $form['element_multiple_elements_header_string'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_header_string',
      '#header' => '{element_multiple_elements_header_string}',
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
        ],
        'email' => [
          '#type' => 'email',
          '#title' => 'email',
        ],
      ],
    ];
    $form['element_multiple_elements_header_true'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_header_true',
      '#header' => TRUE,
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
        ],
        'email' => [
          '#type' => 'email',
          '#title' => 'email',
        ],
      ],
    ];
    $form['element_multiple_elements_header_custom'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_header_custom',
      '#header' => [
        '{textfield_custom}',
        '{email_custom}',
      ],
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
        ],
        'email' => [
          '#type' => 'email',
          '#title' => 'email',
        ],
      ],
    ];
    $form['element_multiple_elements_header_true_label'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_header_true_label',
      '#header' => TRUE,
      '#header_label' => '{element_multiple_elements_header_true_label}',
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
        ],
        'email' => [
          '#type' => 'email',
          '#title' => 'email',
        ],
      ],
    ];
    $form['element_multiple_elements_header_false_label'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_header_false_label',
      '#header' => FALSE,
      '#header_label' => '{element_multiple_elements_header_false_label}',
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
        ],
        'email' => [
          '#type' => 'email',
          '#title' => 'email',
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

}
