<?php

namespace Drupal\element_multiple_test\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\element_multiple_test\TestElementMultipleFormBase;

/**
 * Provides a test form for element multiple.
 */
class TestElementMultipleForm extends TestElementMultipleFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_element_multiple';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['element_multiple_default'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_default',
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_no_sorting'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_no_sorting',
      '#sorting' => FALSE,
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_no_operations'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_no_operations',
      '#operations' => FALSE,
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_no_add_more'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_no_add_more',
      '#add_more' => FALSE,
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_no_add_more_input'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_no_add_more_input',
      '#add_more_input' => FALSE,
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_custom_label'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_custom_label',
      '#add_more_button_label' => '{add_more_button_label}',
      '#add_more_input_label' => '{add_more_input_label}',
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_required'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_required',
      '#element' => [
        '#type' => 'textfield',
        '#title' => 'textfield',
        '#required' => TRUE,
      ],
      '#default_value' => [
        'One',
        'Two',
        'Three',
      ],
    ];
    $form['element_multiple_email_five'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_email_five',
      '#cardinality' => 5,
      '#element' => [
        '#type' => 'email',
        '#title' => 'Please enter an email address',
        '#title_display' => 'invisible',
        '#placeholder' => 'Enter email address',
      ],
      '#default_value' => [
        'example@example.com',
        'test@test.com',
      ],
    ];
    $form['element_multiple_datelist'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_datelist',
      '#element' => [
        '#type' => 'datelist',
      ],
    ];
    $form['element_multiple_elements_name_item'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_name_item',
      '#element' => [
        'first_name' => [
          '#type' => 'textfield',
          '#title' => 'first_name',
        ],
        'last_name' => [
          '#type' => 'textfield',
          '#title' => 'last_name',
        ],
      ],
      '#default_value' => [
        [
          'first_name' => 'John',
          'last_name' => 'Smith',
        ],
        [
          'first_name' => 'Jane',
          'last_name' => 'Doe',
        ],
      ],
    ];
    $form['element_multiple_elements_name_table'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_name_table',
      '#header' => TRUE,
      '#element' => [
        'first_name' => [
          '#type' => 'textfield',
          '#title' => 'first_name',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter first name',
        ],
        'last_name' => [
          '#type' => 'textfield',
          '#title' => 'last_name',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter last name',
        ],
      ],
      '#default_value' => [
        [
          'first_name' => 'John',
          'last_name' => 'Smith',
        ],
        [
          'first_name' => 'Jane',
          'last_name' => 'Doe',
        ],
      ],
    ];
    $form['element_multiple_options'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_options',
      '#header' => TRUE,
      '#label' => 'option',
      '#labels' => 'options',
      '#element' => [
        'value' => [
          '#type' => 'textfield',
          '#title' => 'value',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter value',
        ],
        'text' => [
          '#type' => 'textfield',
          '#title' => 'text',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter text',
        ],
      ],
      '#default_value' => [
        [
          'value' => 'one',
          'text' => 'One',
        ],
        [
          'value' => 'two',
          'text' => 'Two',
        ],
      ],
    ];
    $form['element_multiple_key'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_key',
      '#key' => 'value',
      '#header' => TRUE,
      '#label' => 'option',
      '#labels' => 'options',
      '#element' => [
        'value' => [
          '#type' => 'textfield',
          '#title' => 'value',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter value',
        ],
        'text' => [
          '#type' => 'textfield',
          '#title' => 'text',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter text',
        ],
        'score' => [
          '#type' => 'number',
          '#title' => 'score',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter score',
        ],
      ],
      '#default_value' => [
        'one' => [
          'text' => 'One',
          'score' => 1,
        ],
        'two' => [
          'text' => 'Two',
          'score' => 2,
        ],
      ],
    ];
    $form['element_multiple_elements_hidden_table'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_hidden_table',
      '#header' => TRUE,
      '#element' => [
        'id' => [
          '#type' => 'value',
        ],
        'first_name' => [
          '#type' => 'textfield',
          '#title' => 'first_name',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter first name',
        ],
        'last_name' => [
          '#type' => 'textfield',
          '#title' => 'last_name',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter last name',
        ],
      ],
      '#default_value' => [
        [
          'id' => 'john',
          'first_name' => 'John',
          'last_name' => 'Smith',
        ],
        [
          'id' => 'jane',
          'first_name' => 'Jane',
          'last_name' => 'Doe',
        ],
      ],
    ];
    $form['element_multiple_elements_flattened'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_elements_flattened',
      '#header' => TRUE,
      '#element' => [
        'value' => [
          '#type' => 'textfield',
          '#title' => 'value',
          '#title_display' => 'invisible',
          '#placeholder' => 'Enter value',
        ],
        'option' => [
          '#type' => 'container',
          '#title' => 'text/description',
          '#title_display' => 'invisible',
          'text' => [
            '#type' => 'textfield',
            '#title' => 'text',
            '#title_display' => 'invisible',
            '#placeholder' => 'Enter text',
          ],
          'description' => [
            '#type' => 'textfield',
            '#title' => 'value',
            '#title_display' => 'invisible',
            '#placeholder' => 'Enter description',
          ],
        ],
      ],
      '#default_value' => [
        [
          'value' => 'one',
          'text' => 'One',
          'description' => 'This is the number 1.',
        ],
        [
          'value' => 'two',
          'text' => 'Two',
          'description' => 'This is the number 2.',
        ],
      ],
    ];
    $form['element_multiple_no_items'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_no_items',
      '#min_items' => 0,
      '#empty_items' => 0,
    ];
    $form['element_multiple_custom_attributes'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_custom_attributes',
      '#ajax_attributes' => [
        'class' => ['custom-ajax'],
      ],
      '#table_wrapper_attributes' => [
        'class' => ['custom-table-wrapper'],
      ],
      '#table_attributes' => [
        'class' => ['custom-table'],
      ],
      '#header' => TRUE,
      '#element' => [
        'textfield' => [
          '#type' => 'textfield',
          '#title' => 'textfield',
          '#label_attributes' => [
            'class' => ['custom-label'],
          ],
          '#wrapper_attributes' => [
            'class' => ['custom-wrapper'],
          ],
          '#attributes' => [
            'class' => ['custom-element'],
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

}
