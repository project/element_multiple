<?php

namespace Drupal\element_multiple_test\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\element_multiple_test\TestElementMultipleFormBase;

/**
 * Provides a test form for element multiple no items.
 */
class TestElementMultipleNoItemsForm extends TestElementMultipleFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_element_multiple_no_items';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['element_multiple_no_items'] = [
      '#type' => 'element_multiple',
      '#title' => 'element_multiple_no_items',
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

}
