<?php

namespace Drupal\Tests\element_multiple\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for element multiple.
 *
 * @group element_multiple
 */
class ElementMultipleTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['element_multiple_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests multiple element values.
   */
  public function testMultipleValues() {
    $this->drupalGet('element-multiple-test/test-element-multiple');
    $this->submitForm([], 'Submit');

    $this->assertSession()->responseContains("element_multiple_default:
  - One
  - Two
  - Three
element_multiple_no_sorting:
  - One
  - Two
  - Three
element_multiple_no_operations:
  - One
  - Two
  - Three
element_multiple_no_add_more:
  - One
  - Two
  - Three
element_multiple_no_add_more_input:
  - One
  - Two
  - Three
element_multiple_custom_label:
  - One
  - Two
  - Three
element_multiple_required:
  - One
  - Two
  - Three
element_multiple_email_five:
  - example@example.com
  - test@test.com
element_multiple_datelist: {  }
element_multiple_elements_name_item:
  -
    first_name: John
    last_name: Smith
  -
    first_name: Jane
    last_name: Doe
element_multiple_elements_name_table:
  -
    first_name: John
    last_name: Smith
  -
    first_name: Jane
    last_name: Doe
element_multiple_options:
  -
    value: one
    text: One
  -
    value: two
    text: Two
element_multiple_key:
  one:
    text: One
    score: '1'
  two:
    text: Two
    score: '2'
element_multiple_elements_hidden_table:
  -
    first_name: John
    last_name: Smith
    id: john
  -
    first_name: Jane
    last_name: Doe
    id: jane
element_multiple_elements_flattened:
  -
    value: one
    text: One
    description: 'This is the number 1.'
  -
    value: two
    text: Two
    description: 'This is the number 2.'
element_multiple_no_items: {  }
element_multiple_custom_attributes: {  }");
  }

  /**
   * Tests multiple element render.
   */
  public function testMultipleRenderer() {
    $this->drupalGet('element-multiple-test/test-element-multiple');

    // Check first tr.
    $this->assertSession()->responseContains('<tr class="draggable" data-drupal-selector="edit-element-multiple-default-items-0">');
    $this->assertSession()->responseContains('<td><div class="js-form-item form-item js-form-type-textfield form-item-element-multiple-default-items-0--item- js-form-item-element-multiple-default-items-0--item- form-no-label">');
    $this->assertSession()->responseContains('<label for="edit-element-multiple-default-items-0-item-" class="visually-hidden">Item value</label>');
    $this->assertSession()->responseContains('<input data-drupal-selector="edit-element-multiple-default-items-0-item-" type="text" id="edit-element-multiple-default-items-0-item-" name="element_multiple_default[items][0][_item_]" value="One" size="60" maxlength="128" placeholder="Enter value…" class="form-text" />');
    $this->assertSession()->responseContains('<td class="element-multiple-table--weight"><div class="element-multiple-table--weight js-form-item form-item js-form-type-number form-item-element-multiple-default-items-0-weight js-form-item-element-multiple-default-items-0-weight form-no-label">');
    $this->assertSession()->responseContains('<label for="edit-element-multiple-default-items-0-weight" class="visually-hidden">Item weight</label>');
    $this->assertSession()->responseContains('<input class="element-multiple-sort-weight form-number" data-drupal-selector="edit-element-multiple-default-items-0-weight" type="number" id="edit-element-multiple-default-items-0-weight" name="element_multiple_default[items][0][weight]" value="0" step="1" size="10" />');
    $this->assertSession()->responseContains('<td class="element-multiple-table--operations element-multiple-table--operations-two"><input data-drupal-selector="edit-element-multiple-default-items-0-operations-add" formnovalidate="formnovalidate" type="image" id="edit-element-multiple-default-items-0-operations-add" name="element_multiple_default_table_add_0"');
    $this->assertSession()->responseContains('<input data-drupal-selector="edit-element-multiple-default-items-0-operations-remove" formnovalidate="formnovalidate" type="image" id="edit-element-multiple-default-items-0-operations-remove" name="element_multiple_default_table_remove_0"');

    // Check that sorting is disabled.
    $this->assertSession()->responseNotContains('<tr class="draggable" data-drupal-selector="edit-element-multiple-no-sorting-items-0">');
    $this->assertSession()->responseContains('<tr data-drupal-selector="edit-element-multiple-no-sorting-items-0">');

    // Check that add more is removed.
    $this->assertSession()->fieldExists('element_multiple_no_operations[add][more_items]');
    $this->assertSession()->buttonNotExists('element_multiple_no_add_more_table_add');
    $this->assertSession()->fieldNotExists('element_multiple_no_add_more[add][more_items]');

    // Check that add more input is removed.
    $this->assertSession()->buttonExists('element_multiple_no_add_more_input_table_add');
    $this->assertSession()->fieldNotExists('element_multiple_no_add_more_input[add][more_items]');

    // Check custom labels.
    $this->assertSession()->responseContains('<input data-drupal-selector="edit-element-multiple-custom-label-add-submit" formnovalidate="formnovalidate" type="submit" id="edit-element-multiple-custom-label-add-submit" name="element_multiple_custom_label_table_add" value="{add_more_button_label}" class="button js-form-submit form-submit" />');
    $this->assertSession()->responseContains('<span class="field-suffix">{add_more_input_label}</span>');

    // Check that operations is disabled.
    $this->assertSession()->responseNotContains('data-drupal-selector="edit-element-multiple-no-operations-items-0-operations-remove"');

    // Check no items message.
    $this->assertSession()->responseContains('No items entered. Please add items below.');

    // Check that required does not include any empty elements.
    $this->assertSession()->fieldExists('element_multiple_required[items][2][_item_]');
    $this->assertSession()->fieldNotExists('element_multiple_required[items][3][_item_]');

    // Check custom label, wrapper, and element attributes.
    $this->assertSession()->responseContains('<div class="custom-ajax" id="element_multiple_custom_attributes_table">');
    $this->assertSession()->responseContains('<div class="custom-table-wrapper element-multiple-table">');
    $this->assertSession()->responseContains('<table class="custom-table responsive-enabled" data-drupal-selector="edit-element-multiple-custom-attributes-items" id="edit-element-multiple-custom-attributes-items" data-striping="1">');
    $this->assertSession()->responseContains('<th class="custom-label element_multiple_custom_attributes-table--textfield element-multiple-table--textfield">textfield</th>');
    $this->assertSession()->responseContains('<label class="custom-label visually-hidden"');
    $this->assertSession()->responseContains('<div class="custom-wrapper js-form-item form-item');
    $this->assertSession()->responseContains('<input class="custom-element form-text"');
  }

  /**
   * Tests multiple element validation.
   */
  public function testMultipleValidation() {
    // Check unique #key validation.
    $this->drupalGet('element-multiple-test/test-element-multiple');
    $edit = ['element_multiple_key[items][1][value]' => 'one'];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->responseContains('The <em class="placeholder">value</em> \'one\' is already in use. It must be unique.');

    // Check populated 'element_multiple_default'.
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][0][_item_]', 'One');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][1][_item_]', 'Two');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][2][_item_]', 'Three');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][3][_item_]', '');
    $this->assertSession()->fieldNotExists('element_multiple_default[items][4][_item_]');

    // Check adding empty after one.
    $this->submitForm($edit, 'element_multiple_default_table_add_0');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][0][_item_]', 'One');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][1][_item_]', '');
    $this->assertSession()->fieldValueNotEquals('element_multiple_default[items][1][_item_]', 'Two');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][2][_item_]', 'Two');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][3][_item_]', 'Three');

    // Check removing empty after one.
    $this->submitForm($edit, 'element_multiple_default_table_remove_1');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][0][_item_]', 'One');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][1][_item_]', 'Two');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][2][_item_]', 'Three');

    // Check adding 'four' and 1 more option.
    $edit = ['element_multiple_default[items][3][_item_]' => 'Four'];
    $this->submitForm($edit, 'element_multiple_default_table_add');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][3][_item_]', 'Four');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][4][_item_]', '');

    // Check add 10 more rows.
    $edit = ['element_multiple_default[add][more_items]' => 10];
    $this->submitForm($edit, 'element_multiple_default_table_add');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][14][_item_]', '');
    $this->assertSession()->fieldNotExists('element_multiple_default[items][15][_item_]');

    // Check remove 'one' options.
    $this->submitForm($edit, 'element_multiple_default_table_remove_0');
    $this->assertSession()->fieldNotExists('element_multiple_default[items][14][_item_]');
    $this->assertSession()->fieldValueNotEquals('element_multiple_default[items][0][_item_]', 'One');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][0][_item_]', 'Two');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][1][_item_]', 'Three');
    $this->assertSession()->fieldValueEquals('element_multiple_default[items][2][_item_]', 'Four');

    // Add one options to 'element_multiple_no_items'.
    $this->submitForm($edit, 'element_multiple_no_items_table_add');
    $this->assertSession()->responseNotContains('No items entered. Please add items below.');
    $this->assertSession()->fieldExists('element_multiple_no_items[items][0][_item_]');

    // Check no items message is never displayed when #required.
    $this->drupalGet('element-multiple-test/test-element-multiple-no-items');
    $this->assertSession()->responseNotContains('No items entered. Please add items below.');
    $edit = ['element_multiple_no_items[add][more_items]' => 10];
    $this->submitForm($edit, 'element_multiple_no_items_table_remove_0');
    $this->assertSession()->responseNotContains('No items entered. Please add items below.');
  }

  /**
   * Tests multiple element header.
   */
  public function testMultipleHeader() {
    $this->drupalGet('element-multiple-test/test-element-multiple-header');

    // Check #header property as string.
    $this->assertSession()->responseContains('<th colspan="5">{element_multiple_basic_header_string}</th>');

    // Check #header_label property.
    $this->assertSession()->responseContains('<th colspan="5">{element_multiple_basic_header_label}</th>');

    // Check #header property as string with elements.
    $this->assertSession()->responseContains('<th colspan="6">{element_multiple_elements_header_string}</th>');

    // Check #header property set to true.
    $this->assertSession()->responseContains('<th class="element_multiple_elements_header_true-table--handle element-multiple-table--handle">');
    $this->assertSession()->responseContains('<th class="element_multiple_elements_header_true-table--textfield element-multiple-table--textfield">');
    $this->assertSession()->responseContains('<th class="element_multiple_elements_header_true-table--email element-multiple-table--email">');
    $this->assertSession()->responseContains('<th class="element_multiple_elements_header_true-table--weight element-multiple-table--weight">');
    $this->assertSession()->responseContains('<th class="element_multiple_elements_header_true-table--operations element-multiple-table--operations">');

    // Check #header property set to custom array of header string.
    $this->assertSession()->responseContains('<th>{textfield_custom}</th>');
    $this->assertSession()->responseContains('<th>{email_custom}</th>');

    // Check #header property set to true with header label.
    $this->assertSession()->responseContains('<th colspan="6">{element_multiple_elements_header_true_label}</th>');

    // Check #header property set to false with header label.
    $this->assertSession()->responseContains('<th colspan="5">{element_multiple_elements_header_false_label}</th>');
  }

}
